```
Nama  : Roby Awaludin Fajar

NIM   : 222011392

Kelas : 3SI3
```

## **Android Storage**

SQLite merupakan sebuah sistem manajemen basisdata relasional yang
bersifat ACID-compliant dan memiliki ukuran pustaka kode yang relatif kecil, ditulis
dalam bahasa C. SQLite merupakan jenis database yang ringan dan tersedia dalam OS
Android. Pada praktikum kali ini kita akan mempraktekkan android storage
menggunakan SQLite, lengkap dengan proses CRUD dan menerapkan RecyclerView
yang sudah dipelajari sebelumnya.

### Screenshot Penugasan

1. Screenshot tambah data `mahasiswa`

- Menambah data mahasiswa dengan nama `Roby Awaludin Fajar`
  ![Gambar 1](ss/ss1.png)
  ![Gambar 2](ss/ss2.png)
- Menambah data mahasiswa dengan nama `Nanda Putri Karizki`
  ![Gambar 3](ss/ss3.png)
  ![Gambar 4](ss/ss4.png)

2. Screenshot lihat data `mahasiswa`
   ![Gambar 5](ss/ss5.png)

3. Screenshot update data `mahasiswa`

- Update data mahasiswa dengan nama `Roby Awaludin Fajar` menjadi `Roby Awaludin Fajar Update`
  ![Gambar 6](ss/ss6.png)
  ![Gambar 7](ss/ss7.png)
- Melihat data mahasiswa setelah diupdate
  ![Gambar 8](ss/ss8.png)

4. Screenshot hapus data `mahasiswa`

- Hapus data mahasiswa dengan nama `Aprilia Regina`
  ![Gambar 9](ss/ss9.png)
  ![Gambar 10](ss/ss10.png)
- Melihat data mahasiswa setelah ada satu mahasiswa yang `didelete/dihapus`
  ![Gambar 11](ss/ss11.png)
